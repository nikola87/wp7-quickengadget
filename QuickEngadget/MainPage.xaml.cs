﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;
using Engadget.Model;
using System.Windows.Navigation;
using System.Windows.Data;
using System.Collections.ObjectModel;
using System.Windows.Threading;

using Engadget.ViewModel;

namespace Engadget
{
    public partial class MainPage : PhoneApplicationPage
    {
        private bool animationStarted = false;

        private MainPageViewModel mainPageModelView;

        private static string destination = "/View/ArticlesPage.xaml?type=";

        // Constructor
        public MainPage()
        {
            InitializeComponent();

            mainPageModelView = new MainPageViewModel(Orientation);
            DataContext = mainPageModelView;
            //on page load
            //Loaded += new RoutedEventHandler(MainPage_Loaded);
        }

        private void MainPage_OrientationChanged(object sender, OrientationChangedEventArgs e)
        {
            mainPageModelView.SetButtonsVisibility(e.Orientation);
        }

        void OnButtonClick(object sender, RoutedEventArgs args)
        {

            if (animationStarted == true)
                return;

            animationStarted = true;

            if (sender == EngadgetButton)
            {
                buttonRotateStorybardEngadget.Completed += new EventHandler(buttonRotateStorybardEngadget_Completed);
                buttonRotateStorybardEngadget.Begin();
            }

            else if (sender == EngadgetMobButton)
            {
                buttonRotateStorybardMobile.Completed += new EventHandler(buttonRotateStorybardMobile_Completed);
                buttonRotateStorybardMobile.Begin();
            }

            else if (sender == EngadgetHDButton)
            {
                buttonRotateStorybardHD.Completed += new EventHandler(buttonRotateStorybardHD_Completed);
                buttonRotateStorybardHD.Begin();

            }

            else if (sender == EngadgetAltButton)
            {
                buttonRotateStorybardAlt.Completed += new EventHandler(buttonRotateStorybardAlt_Completed);
                buttonRotateStorybardAlt.Begin();
            }

            else if (sender == ArchiveButton)
            {
                buttonRotateStorybardArchive.Completed += new EventHandler(buttonRotateStorybardArchive_Completed);
                buttonRotateStorybardArchive.Begin();
            }

            else if (sender == AboutButton)
            {
                buttonRotateStorybardAbout.Completed += new EventHandler(buttonRotateStorybardAbout_Completed);
                buttonRotateStorybardAbout.Begin();

            }
        }


        private void buttonRotateStorybardEngadget_Completed(object sender, EventArgs e)
        {
            animationStarted = false;
            App.NavigatedFrom = (int)App.NavigatedFromPages.MainPage;
            NavigationService.Navigate(new Uri(destination + ArticlesPageViewModel.ENGADGET_PAGE_TYPE, UriKind.Relative));         
        }

        private void buttonRotateStorybardMobile_Completed(object sender, EventArgs e)
        {
            animationStarted = false;
            App.NavigatedFrom = (int)App.NavigatedFromPages.MainPage;
            NavigationService.Navigate(new Uri(destination + ArticlesPageViewModel.MOBILE_PAGE_TYPE, UriKind.Relative));
        }

        private void buttonRotateStorybardHD_Completed(object sender, EventArgs e)
        {
            animationStarted = false;
            App.NavigatedFrom = (int)App.NavigatedFromPages.MainPage;
            NavigationService.Navigate(new Uri(destination + ArticlesPageViewModel.HD_PAGE_TYPE, UriKind.Relative));
        }

        private void buttonRotateStorybardAlt_Completed(object sender, EventArgs e)
        {
            animationStarted = false;
            App.NavigatedFrom = (int)App.NavigatedFromPages.MainPage;
            NavigationService.Navigate(new Uri(destination + ArticlesPageViewModel.ALT_PAGE_TYPE, UriKind.Relative));
        }

        private void buttonRotateStorybardArchive_Completed(object sender, EventArgs e)
        {
            animationStarted = false;
            NavigationService.Navigate(new Uri("/View/ArchivePage.xaml", UriKind.Relative));
        }

        private void buttonRotateStorybardAbout_Completed(object sender, EventArgs e)
        {
            animationStarted = false;
            NavigationService.Navigate(new Uri("/View/ABoutPage.xaml", UriKind.Relative));
        }

    }
}