﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.ComponentModel;

namespace Engadget.Model
{
    public class Article : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        private string title;
        public string Title
        {
            get { return title; }
            set
            {
                title = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Title"));
            }
        }

        private string author;
        public string Author
        {
            get { return author;}
            set
            {
                author = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Author"));
            }
        }


        private string url;
        public string Url
        {
            get{ return url; }
            set
            {
                url = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Url"));
            }
        }

        private string imgUrl;
        public string ImgUrl
        {
            get { return imgUrl; }
            set
            {
                imgUrl = value;
                OnPropertyChanged(new PropertyChangedEventArgs("ImgUrl"));
            }
        }

        private string datetime;
        public string Datetime
        {
            get { return datetime; }
            set
            {
                datetime = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Datetime"));
            }

        }

        private string content;
        public string Content
        {
            get { return content; }
            set
            { 
                content = value;
                OnPropertyChanged(new PropertyChangedEventArgs("Content"));
            }

        }

        public Article()
        {
            this.Title = "";
            this.Url = "";
            this.ImgUrl = "";
            this.Datetime = "";
            this.Content = "";
        }

        public Article(string title, string imgUrl, string url, string datetime, string content)
        {
            this.Title = title;
            this.Url = url;
            this.ImgUrl = imgUrl;
            this.Datetime = datetime;
            this.Content = content;
        }

        public Article(Article art)
        {
            Title = art.Title;
            Url = art.Url;
            ImgUrl = art.ImgUrl;
            Datetime = art.Datetime;
            Content = art.content;
        }

        protected virtual void OnPropertyChanged(PropertyChangedEventArgs args)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, args);
        }

    }
}
