﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Diagnostics;
using System.IO;
using System.ComponentModel;
using System.Xml.Linq;
using System.Xml;
using System.Linq;
using System.Xml.Serialization;
using System.Collections.Generic;

using Engadget.ViewModel;

namespace Engadget.Model
{
    public class GetFeed
    {
        private string xmlFile;
        private ArticlesPageViewModel viewModel;

        public GetFeed(ArticlesPageViewModel viewModel, string url)
        {
            this.viewModel = viewModel;

            try
            {
                this.viewModel.LoadingStackVisible = Visibility.Visible;
                WebClient webClient = new WebClient();
                webClient.OpenReadCompleted += OnXmlReceived;
                webClient.OpenReadAsync(new Uri(url));

            }
            catch (Exception ex)
            {
                Debug.WriteLine("GetResults.Next() WRONG URL: " + ex.ToString());
            }
        }

        private void OnXmlReceived(object sender, OpenReadCompletedEventArgs args)
        {
            if (args.Error != null)
                return;

            StreamReader reader = new StreamReader(args.Result);
            xmlFile = reader.ReadToEnd();

            //start worker thread
            BackgroundWorker worker = new BackgroundWorker();
            worker.WorkerReportsProgress = true;
            worker.WorkerSupportsCancellation = true;
            worker.DoWork += new DoWorkEventHandler(DoParse);
            worker.RunWorkerCompleted += new RunWorkerCompletedEventHandler(EndParsing);
            worker.RunWorkerAsync();
        }

        private void DoParse(object sender, DoWorkEventArgs e)
        {

            XElement xmlItems;
            try
            {
                xmlItems = XElement.Parse(xmlFile);
                IEnumerable<Article> ac = from x in xmlItems.Descendants("item")
                                          select new Article //new Article 
                                          {
                                              Content = x.Element("description").Value,
                                              Url = x.Element("link").Value,
                                              ImgUrl = GetImgUrlOfArticle(x.Element("description").Value),
                                              Datetime = x.Element("pubDate").Value,
                                              Title = x.Element("title").Value 
                                          };

                foreach (Article art in ac)
                {
                    {
                        Article temp = new Article(art);
                        App.articlesPage.Dispatcher.BeginInvoke(new Action(delegate()
                        {
                            viewModel.Articles.Add(temp);
                        }));
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("GetFeed.OnDownloadRssCompletedXElement.Parse: " + ex.ToString());
            }

        }

        private void EndParsing(object sender, RunWorkerCompletedEventArgs e)
        {
            viewModel.LoadingStackVisible = Visibility.Collapsed;
        }

        private string GetImgUrlOfArticle(string content)
        {
            string result = content.Substring(content.IndexOf("<img"));
            result = result.Substring(result.IndexOf("src=\"") + 5);
            result = result.Substring(0, result.IndexOf("\""));

            return result;
        }

    }
}
