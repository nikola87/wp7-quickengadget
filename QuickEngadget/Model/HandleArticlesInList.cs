﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.ObjectModel;
using System.IO.IsolatedStorage;
using System.Xml.Serialization;
using System.Diagnostics;
using System.IO;
using Engadget.ViewModel;

namespace Engadget.Model
{
    public static class HandleArticlesInList
    {
        public static void SaveArticles(this ObservableCollection<Article> articles, string fileName)
        {
            try
            {
                IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication();
                IsolatedStorageFileStream stream = storage.CreateFile(fileName);
                XmlSerializer xml = new XmlSerializer(articles.GetType());
                xml.Serialize(stream, articles);
                stream.Close();
                stream.Dispose();
            }
            catch (Exception e)
            {
                Debug.WriteLine("SaveArticles() exception" + e.ToString());
            }
        }

        public static ObservableCollection<Article> LoadArticles(this ObservableCollection<Article> articles, string fileName)
        {
            try
            {
                ObservableCollection<Article> collection = new ObservableCollection<Article>();
                IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication();
                if (storage.FileExists(fileName))
                {
                    IsolatedStorageFileStream stream =
                    storage.OpenFile(fileName, FileMode.Open);
                    XmlSerializer xml = new XmlSerializer(typeof(ObservableCollection<Article>));
                    collection = xml.Deserialize(stream) as ObservableCollection<Article>;
                    stream.Close();
                    stream.Dispose();
                }

                return collection;
            }
            catch (Exception e)
            {
                Debug.WriteLine("LoadArticles() exception" + e.ToString());
                return null;
            }
        }

        public static bool IsExist(this ObservableCollection<Article> articles, Article item)
        {
            if (articles.Count == 0)
                return false;

            bool exist = false;

            foreach (Article art in articles)
            {
                if (art.Title.Equals(item.Title))
                {
                    exist = true;
                    break;
                }
            }

            return exist;
        }

        public static int IndexOfArticle(this ObservableCollection<Article> articles, Article item)
        {
            int index = -1;

            for (int i = 0; i < articles.Count; i++)
            {
                if(articles[i].Title.Equals(item.Title))
                {
                    index = i;
                    break;
                }
            }

            return index;
        }

        //Important -- this method loads list from memory!!!
        public static void AddArticle(this ObservableCollection<Article> articles, Article item, string type)
        {
            string file = GetFileName(type);
            articles = articles.LoadArticles(file);
            if(!articles.IsExist(item))
                articles.Add(item);
            articles.SaveArticles(file);
        }

        public static void RemoveArticle(this ObservableCollection<Article> articles, Article item, string type)
        {
           int index = articles.IndexOfArticle(item);

            if(index != -1)
                articles.RemoveAt(index);

            string file = GetFileName(type);
            articles.SaveArticles(file);
        }

        public static void RemoveAllArticles(this ObservableCollection<Article> articles, string type)
        {
            articles.Clear();
            DeleteFile(type);
        }

        private static string GetFileName(string type)
        {
            string file = "";
            switch (type)
            {
                default:
                case ArticlesPageViewModel.ENGADGET_PAGE_TYPE:
                    file = App.ENGADGET_ARCHIVE_FILE;
                    break;

                case ArticlesPageViewModel.MOBILE_PAGE_TYPE:
                    file = App.ENGADGET_MOBILE_ARCHIVE_FILE;
                    break;

                case ArticlesPageViewModel.HD_PAGE_TYPE:
                    file = App.ENGADGET_HD_ARCHIVE_FILE;
                    break;

                case ArticlesPageViewModel.ALT_PAGE_TYPE:
                    file = App.ENGADGET_ALT_ARCHIVE_FILE;
                    break;
            }

            return file;
        }

        private static void DeleteFile(string type)
        {
            string file = GetFileName(type);

            try
            {
                IsolatedStorageFile storage = IsolatedStorageFile.GetUserStoreForApplication();
                storage.DeleteFile(file);
            }
            catch (Exception e)
            {
                Debug.WriteLine("DeleteFile exception" + e.ToString());
            }

        }

    }
}
