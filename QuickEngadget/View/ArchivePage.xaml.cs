﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Microsoft.Phone.Controls;

using Engadget.ViewModel;
using Engadget.Model;
using System.Diagnostics;
using System.Windows.Navigation;

namespace Engadget.View
{
    public partial class ArchivePage : PhoneApplicationPage
    {
        private ArchivePageViewModel viewModel;

        public ArchivePage()
        {
            InitializeComponent();

            viewModel = new ArchivePageViewModel();

            DataContext = viewModel;
            EngadgetList.ItemsSource = viewModel.Engadget;
            MobileList.ItemsSource = viewModel.Mobile;
            HdList.ItemsSource = viewModel.Hd;
            AltList.ItemsSource = viewModel.Alt;
        }

        protected override void OnNavigatedTo(NavigationEventArgs args)
        {
            switch (App.NavigatedFrom)
            {
                case (int)App.NavigatedFromPages.ArticlePage:
                    FreeSelection();
                    break;
            }


            base.OnNavigatedTo(args);
        }

        private void OnEngadgetRemoveMenuClick(object sender, RoutedEventArgs e)
        {
            ListBoxItem selectedListBoxItem = EngadgetList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            if (selectedListBoxItem == null)
                return;

            Article item = (Article)selectedListBoxItem.Content;

            viewModel.Engadget.RemoveArticle(item, ArticlesPageViewModel.ENGADGET_PAGE_TYPE);
        }

        private void OnMobileRemoveMenuClick(object sender, RoutedEventArgs e)
        {
            ListBoxItem selectedListBoxItem = MobileList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            if (selectedListBoxItem == null)
                return;

            Article item = (Article)selectedListBoxItem.Content;

            viewModel.Mobile.RemoveArticle(item, ArticlesPageViewModel.MOBILE_PAGE_TYPE);
        }

        private void OnHdRemoveMenuClick(object sender, RoutedEventArgs e)
        {
            ListBoxItem selectedListBoxItem = HdList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            if (selectedListBoxItem == null)
                return;

            Article item = (Article)selectedListBoxItem.Content;

            viewModel.Hd.RemoveArticle(item, ArticlesPageViewModel.HD_PAGE_TYPE);
        }

        private void OnAltRemoveMenuClick(object sender, RoutedEventArgs e)
        {
            ListBoxItem selectedListBoxItem = AltList.ItemContainerGenerator.ContainerFromItem((sender as MenuItem).DataContext) as ListBoxItem;
            if (selectedListBoxItem == null)
                return;

            Article item = (Article)selectedListBoxItem.Content;

            viewModel.Alt.RemoveArticle(item, ArticlesPageViewModel.ALT_PAGE_TYPE);

        }

        private void OnApplicationBarClearAllButtonClick(object sender, EventArgs e)
        {
            viewModel.Engadget.RemoveAllArticles(ArticlesPageViewModel.ENGADGET_PAGE_TYPE);
            viewModel.Mobile.RemoveAllArticles(ArticlesPageViewModel.MOBILE_PAGE_TYPE);
            viewModel.Hd.RemoveAllArticles(ArticlesPageViewModel.HD_PAGE_TYPE);
            viewModel.Alt.RemoveAllArticles(ArticlesPageViewModel.ALT_PAGE_TYPE);
        }

        private void ArticlesList_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ListBoxItem selectedItem = (sender as ListBox).ItemContainerGenerator.ContainerFromItem((sender as ListBox).SelectedItem) as ListBoxItem;
            if (selectedItem == null)
                return;

            App.articleToDisplay = (Article)selectedItem.Content;
            NavigationService.Navigate(new Uri("/View/ArticlePage.xaml", UriKind.Relative));
        }

        private void FreeSelection()
        {
            try
            {
                EngadgetList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            try
            {
                MobileList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            try
            {
                HdList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }

            try
            {
                AltList.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.ToString());
            }
        }
    }
}

